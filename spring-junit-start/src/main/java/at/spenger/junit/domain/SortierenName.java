package at.spenger.junit.domain;

import java.util.Comparator;

public class SortierenName implements Comparator<Person> {

	/*
	 * Compare methode für das Sortieren nach Name.
	 * @return ware1.compareTo(ware2)
	 */
	 @Override
	public int compare(Person p1, Person p2) {
			
			String s1 = p1.getFirstName() + " " + p1.getLastName();
			String s2 = p2.getFirstName() + " " + p2.getLastName();
			return s1.compareTo(s2);
	}
}
		
