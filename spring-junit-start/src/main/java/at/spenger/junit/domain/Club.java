package at.spenger.junit.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class Club {
	private List<Person> l;
	
	public Club() {
		l = new ArrayList<>();
	}
	
	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}
	
	public boolean leave(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		if(l.contains(p))
			return l.remove(p);
		return false;
	}
	
	public int numberOf() {
		return l.size();
	}
	
	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}
	
	public float getAverage()
	{
		float c = 0;
		for(Person p : l)
		{
			c += LocalDateTime.now().getYear() - p.getBirthday().getYear();
		}
		c = c / l.size();
		
		return c;
	}
	
	public void sortierenName()
	{
		Collections.sort(l, new SortierenName());
	}
	
	public void sortierenAlter()
	{
		Collections.sort(l, new SortierenAlter());
	}
	
	public void ClearClub()
	{
		l.clear();
	}
	
	
	public String print()
	{
		StringBuilder b = new StringBuilder();
		b.append("**************************");
		b.append(System.lineSeparator());
		b.append("There are currently "+l.size() + " persons in the club!");
		b.append(System.lineSeparator());
		for(Person p : l)
		{
			b.append("Name: "+p.getFirstName() + " "+p.getLastName() + ", Age: "+p.getBirthdayString());
			b.append(System.lineSeparator());
		}
		b.append("**************************");
		
		return b.toString();
	}

}
