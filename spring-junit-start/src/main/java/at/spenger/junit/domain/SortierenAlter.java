package at.spenger.junit.domain;

import java.util.Comparator;

public class SortierenAlter implements Comparator<Person> {

	/*
	 * Compare methode für das Sortieren nach Alter.
	 * @return ware1.compareTo(ware2)
	 */
	 @Override
	public int compare(Person p1, Person p2) {
		 
			return p1.getBirthday().toString().compareTo(p2.getBirthday().toString());
	}
}
		
