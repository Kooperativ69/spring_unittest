package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Calendar;
import java.util.Date;

import org.junit.Ignore;
import org.junit.Test;
import org.neo4j.cypher.internal.compiler.v2_1.ast.rewriters.addUniquenessPredicates;
import org.springframework.format.datetime.joda.LocalDateParser;

import at.spenger.junit.domain.Person.Sex;

public class ClubTest {

	@Test
	@Ignore
	public void testEnter() {
		fail("Not yet implemented");
	}

	@Test
	public void testNumberOf() {
		Club c = new Club();
		
		int erg = c.numberOf();
		int erwartet = 0;
		assertEquals("Is the number of 0", erg, erwartet);
	}

	@Test
	public void testGetAverage() {
		Club c = new Club();
		c = addPersons(c);
		
		float erg = c.getAverage();
		System.out.println("Average age: " + erg);
		boolean b = erg > 0;
		assertTrue(b);
	}
	
	@Test
	public void testSortierenNachAlter() {
		Club c = new Club();	
		c= addPersons(c);
		System.out.println(c.print());
		c.sortierenAlter();
		
		boolean b = c.getPersons().get(0).getBirthday().isBefore((c.getPersons().get(c.getPersons().size()-1).getBirthday()));
		System.out.println(c.print());
		
		assertEquals("The first Person should be older than the last one! ", true, b);
	}
	
	@Test
	public void testSortierenNachName() {
		Club c = new Club();	
		c= addPersons(c);
		System.out.println(c.print());
		c.sortierenName();
		
		int x = c.getPersons().get(0).getFullName().compareTo(c.getPersons().get(c.getPersons().size()-1).getFullName());
		System.out.println(c.print());
		boolean b = 0 > x;
		
		System.out.println(x);
		
		assertEquals("The first Person's name should be over the last ones! ", true, b);
	}
	
	@Test
	public void testPersonLeave() {
		Club c = new Club();	
		c= addPersons(c);
		System.out.println(c.print());
		
		DateTimeFormatter fm = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate d = LocalDate.parse("1995-05-05", fm);
		Person p = new Person("Peter", "mom", d, Sex.MALE);
		c.enter(p);
		
		if(c.getPersons().size() == 0)
			c.enter(p);
		boolean b = c.leave(c.getPersons().get(0));
		boolean u = true;
		try{
			 u = c.leave(null);
		}
		catch(IllegalArgumentException e)
		{
			System.out.println(e.getMessage());
			u = false;
		}
		
		
		System.out.println(c.print());

		
		assertNotEquals("The booleans should not be the same! ", b, u);
	}
	
	
	@Test
	public void testClearClub() {
		Club c = new Club();	
		c= addPersons(c);
		int i = c.getPersons().size();
		
		c.ClearClub();
		
		int f = c.getPersons().size();
		
		
		assertNotEquals("The amount should no be the same", i, f);
	}
	
	
	public Club addPersons(Club c)
	{
		
		DateTimeFormatter fm = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		LocalDate d = LocalDate.parse("1995-05-05", fm);
		Person p = new Person("Peter", "mom", d, Sex.MALE);
		c.enter(p);
		
		d = LocalDate.parse("1996-05-02", fm);
		p = new Person("Horst", "Teaman", d, Sex.MALE);
		c.enter(p);
		
		d = LocalDate.parse("1999-08-12", fm);
		p = new Person("Kathi", "Burman", d, Sex.FEMALE);
		c.enter(p);
		
		d = LocalDate.parse("1991-01-02", fm);
		p = new Person("Lena", "Kamar", d, Sex.FEMALE);
		c.enter(p);
		
		return c;
	}

}
